// createElement
const loginBtnEl = document.querySelector("#login-btn");
const clearBtn = document.querySelector('#clear-btn');

// console.log(loginBtn, clearBtn);

// document.addEventListener('click',()=>{

// })

loginBtn.addEventListener('click', () => {
    let userName = document.querySelector('#username').value;
    let password = document.querySelector('#password').value;

    // console.log(userName, password);

    // Element Creation
    let displayElement = document.body;
    let usernameHeading = document.createElement('h1');
    let passwordHeading = document.createElement('p');

    // console.log(userNameHeading, passWordHeading);

    // Value assign

    usernameHeading.innerHTML = `Username : ${userName}`;
    passwordHeading.innerHTML = `Password : ${password}`;

    // Append Elements
    displayElement.append(usernameHeading, passwordHeading);

});

clearBtn.addEventListener('click', () => {
    document.querySelector('#username').value = '';
    document.querySelector('#password').value = '';
})