// getElementById
// getElementByClassName
// getElementByTagName
// querySelector and querySelectorAll

// getElementById

var element = document.getElementById("myElementId");
console.log(element);
console.log(element.innerHTML);
element.innerText="Hello world"
console.log(element.innerText);

var inputText = document.getElementById("input-text");

console.log(inputText);

// getElementsByClassName

let headingText = document.getElementsByClassName('heading');
let selectorText = document.querySelectorAll('.heading');

console.log(headingText);
console.log(`querySelector ${selectorText}`);

// for (let key in headingText){
//     console.log(key.innerText);
// }

// array.map(function(currentValue, index, arr), thisValue)
// Create a new paragraph element
var paragraph = document.createElement("p");

// Check if an element contains another element
var parent = document.getElementById("parent");
var child = document.getElementById("child");
var isContained = parent.contains(child);
console.log(isContained);

// Add the paragraph element to the end of the parent element's children
console.log(parent.appendChild(paragraph));;
console.log(child);
// Remove the child element from the parent element
parent.removeChild(child);