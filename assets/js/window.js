// Window
// BOM - Browser Object Model

// Screen Object

// console.log(window.screen);
// console.log(window.screen.availHeight);
// console.log(window.screen.availWidth);
// console.log(window.innerHeight);
// console.log(window.innerWidth);

// Location Object
// console.log(window.location);
// console.log(location.host);
// console.log(location.href);

// History Object
// console.log(window.history);

// function front(){
//     history.forward();
// }

// function back(){
//     history.back();
// }

// alert - alert()
// confirm - confirm()
// prompt - prompt

// prompt("What is your name?",'Enter your Full Name')

const user = {

    fullName:"",
    age:"",
    greeting(){
        
        document.write(`<h1> Welcome to ${this.fullName}.</h1>`);
    }
}
user.fullName = prompt("What is your name?",'Enter your Full Name');
user.age = prompt("How old are you?",'Enter your age');
user.greeting();
// user.greeting();

// name, age, special/normal