// MouseEvents
// mousedown
// document.addEventListener('mousedown',(e)=>{
//     console.log('mousedown',e);
// });

// mouseup
// document.addEventListener('mouseup',(e)=>{
//     console.log('mouseup',e);
// });

// copy
// document.addEventListener('copy',(e)=>{
//     e.preventDefault();
//     console.log('copy',e);
//     return false
// });

// // mouseover
// document.addEventListener('mouseover',(e)=>{
//     console.log('mouseover',e);
// });

// Chrome
// mouseenter
// document.addEventListener('mouseenter',(e)=>{
//     console.log('mouseenter',e);
// });

// mouseleave
// document.addEventListener('mouseleave',(e)=>{
//     console.log('mouseleave',e);
// });

// mousemove
// document.addEventListener('mousemove,(e)=>{
//     console.log('mousemove',e);
// });

// Keyboard Event

// // keyup
// document.addEventListener('keyup',(e)=>{
//     console.log('keyup',e);
//     console.log(`${e.key}, ${e.code}`);
// });

// // keydown
// document.addEventListener('keydown',(e)=>{
//     console.log('keydown',e);
//     // console.log(`${e.key}, ${e.code}`);
// });

// keypress
document.addEventListener('keypress',(e)=>{
    console.log('keypress',e);
    console.log(`${e.key}, ${e.code}`);
});